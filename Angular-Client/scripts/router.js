'use strict';
angular.module('Client',['ngResource','ngRoute'])
	.config(function($routeProvider) {
		$routeProvider
		.when('Angular-Client/notes',{
			templateUrl: 'views/note/index.html',
			controller: 'IndexNoteCtrl'
		})
		.when('Angular-Client/notes/new',{
			templateUrl: 'views/note/create.html',
			controller: 'CreateNoteCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});
		
	});